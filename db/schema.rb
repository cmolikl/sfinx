# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_04_06_135319) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "chart_queries", force: :cascade do |t|
    t.string "name"
    t.jsonb "params"
    t.bigint "experiment_id"
    t.index ["experiment_id"], name: "index_chart_queries_on_experiment_id"
  end

  create_table "double_data", force: :cascade do |t|
    t.decimal "value"
  end

  create_table "experiment_data", force: :cascade do |t|
    t.integer "target_id"
    t.string "target_type"
    t.bigint "participant_id"
    t.bigint "variable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "delete_reason"
    t.index ["participant_id"], name: "index_experiment_data_on_participant_id"
    t.index ["variable_id"], name: "index_experiment_data_on_variable_id"
  end

  create_table "experiment_json_data", force: :cascade do |t|
    t.bigint "part_id"
    t.bigint "experiment_id"
    t.jsonb "data"
    t.bigint "participant_id"
    t.index ["experiment_id"], name: "index_experiment_json_data_on_experiment_id"
    t.index ["part_id"], name: "index_experiment_json_data_on_part_id"
    t.index ["participant_id"], name: "index_experiment_json_data_on_participant_id"
  end

  create_table "experiment_parts", force: :cascade do |t|
    t.string "access_token"
    t.string "name"
    t.string "description"
    t.integer "design_type"
    t.bigint "experiment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "repetition_count", default: 1
    t.index ["access_token"], name: "index_experiment_parts_on_access_token", unique: true
    t.index ["experiment_id"], name: "index_experiment_parts_on_experiment_id"
  end

  create_table "experiment_variables", force: :cascade do |t|
    t.integer "data_type"
    t.string "name"
    t.bigint "part_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "positive_value"
    t.integer "calculation_method"
    t.index ["part_id"], name: "index_experiment_variables_on_part_id"
  end

  create_table "experiments", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "copy_parent_id"
    t.string "state"
    t.string "access_token"
    t.decimal "timeout"
    t.decimal "number_of_parts"
    t.index ["user_id"], name: "index_experiments_on_user_id"
  end

  create_table "long_data", force: :cascade do |t|
    t.integer "value"
  end

  create_table "participants", force: :cascade do |t|
    t.string "internal_id"
    t.string "external_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "experiment_id"
    t.integer "sequential_id"
    t.decimal "finished_parts"
    t.index ["experiment_id"], name: "index_participants_on_experiment_id"
  end

  create_table "string_data", force: :cascade do |t|
    t.string "value"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "authentication_token", limit: 30
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "experiment_data", "participants"
  add_foreign_key "experiment_json_data", "participants"
  add_foreign_key "participants", "experiments"
end
