class AddExperimentToParticipant < ActiveRecord::Migration[5.1]
  def change
    add_reference :participants, :experiment, foreign_key: true
    add_column :participants, :sequential_id, :integer
  end
end
