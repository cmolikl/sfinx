class AddFinishedPartsToParticipant < ActiveRecord::Migration[5.1]
  def change
    add_column :participants, :finished_parts, :decimal
  end
end
