class AddTimeoutToExperiment < ActiveRecord::Migration[5.1]
  def change
    add_column :experiments, :timeout, :decimal
    add_column :experiments, :number_of_parts, :decimal
  end
end
