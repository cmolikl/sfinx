class AddAccessTokenToExperiment < ActiveRecord::Migration[5.1]
  def change
    add_column :experiments, :access_token, :string
  end
end
