"use strict";
var Transfer = (function () {
    function Transfer() {
    }

    Transfer.registerParticipant = function (experimentToken) {
        // do the registration
        // ajax request
        var promise = $.ajax({
            context: this,
            type: "POST",
            url: Transfer.host + "/api/v1/participants.json",
            data: {
                experiment_id: experimentToken
            },
            dataType: "json",
        });
        promise.done(function (response) {
            // save participant id
            localStorage.setItem("participantId", response.internal_id);
            localStorage.setItem("participantIndex", response.sequential_id-1);
            console.info('Successful registration.');
        });
        promise.fail(function (xhr, status, error) {
            console.error('Error of registration:' + xhr.responseJSON.message);
        });
        return promise;
    };

    Transfer.sendData = function (partToken, params) {
        // ajax request
        var message = {
            type: "POST",
            url: Transfer.host + "/api/v1/experiments/parts/" + partToken + "/data.json",
            contentType: "application/json",
            data: JSON.stringify({
                variable_values: params,
                internal_id: localStorage.getItem("participantId")
            }),
            dataType: "json"
        };
        console.log(JSON.stringify(message));
        var promise = $.ajax(message);
        promise.done(function (response) {
            console.info('Successful data transfer.');
        });
        promise.fail(function (xhr, status, error) {
            console.error('Error of data sending: ' + error + " - " + xhr.responseJSON.message);
        });
        return promise;
    };
    Transfer.host = '';
    return Transfer;
}());
