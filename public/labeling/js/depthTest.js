
var currentSceneCnt = 0;
var currentPosition = 0;
var userAnswer;

var sequence;
var t0, t1;

var depthTime, readabilityTime;
var numOfScenes;

var sequence;

var depthTestData = [];
var textTestData = [];

var depth = function ()
{
	initProperties();

	var test = testSequence[testIndex];
	counterbalanceModel(participantIndex, modelIndex);

	loadJSON(function(response)
	{
		// parse json into object
		var s = JSON.parse(response);
		// get container for images
		var container = document.getElementById('imgContainer');
		numOfScenes = s.length;

		for(var i = 0; i < s.length; i++)
		{
			for (var j = 0; j < s[i].methods.length; j++)
			{
				var imgNode = document.createElement('img');
				imgNode.src = 'img/' + s[i].model + '/' + s[i].methods[j] + '.png';
				imgNode.id = s[i].model + '_' + s[i].methods[j];
				imgNode.style.width = '800px';
				imgNode.style.height = '800px';
				imgNode.style.display = 'none';

				container.appendChild(imgNode);
			}
		}

		sequence = new Array(4);
		for (var i = 0; i < 4; i++)
		{
			sequence[i] = randomSequence(4);
		}

		var scene = s[(model - 1) * 4 + sequence[model - 1][currentPosition]];
		document.getElementById(scene.model + '_' + scene.methods[method - 1]).style.display = '';

		// start time measurement for depth test
		t0 = performance.now();

	},'depth.json');

}

function getText(input)
{
	// choose answer
	if (input == 1)
	{
		// left text is closer
		var right = document.getElementById('rightText');
		right.disabled = true;
		var no = document.getElementById('noText');
		no.disabled = true;
		userAnswer = 'L';
	}
	else if (input == 2)
	{
		// right text is closer
		var left = document.getElementById('leftText');
		left.disabled = true;
		var no = document.getElementById('noText');
		no.disabled = true;
		userAnswer = 'R';
	}
	else if (input == 0)
	{
		var left = document.getElementById('leftText');
		left.disabled = true;
		var right = document.getElementById('rightText');
		right.disabled = true;
		userAnswer = 'N';
	}

	// stop time measuring for depth test
	t1 = performance.now();
	depthTime = t1 - t0;

	loadJSON(function(response)
	{
		// parse json into object
		var s = JSON.parse(response);
		var scene = s[(model - 1) * 4 + sequence[model - 1][currentPosition]];
		var correctAnswer = scene.closer;
		var error = 1;
		if (userAnswer == correctAnswer)
			error = 0;

		// push results
		var data = [];
		data.push({	name: 'depth-time', value: depthTime});
		data.push({	name: 'depth-error', value: error});
		data.push({	name: 'depth-model', value: sceneNames[model - 1]});
		data.push({	name: 'depth-method', value: method - 1});

		Transfer.sendData(partToken, data);
		//depthTestData.push(data);

	}, 'depth.json');

	// create input for texts
		var d = document.getElementById('buttons');
		if (d != null)
			d.outerHTML = '';
		d = document.createElement('div');
		d.setAttribute('id', 'buttons');


	if (currentPosition == 3)
	{

		var textLabel = document.createElement('label');
		textLabel.setAttribute('id', 'readabilityLabel');
		textLabel.setAttribute('class', 'white-text');
		textLabel.appendChild(document.createTextNode('Rewrite texts from image'));
		var lLabel = document.createElement('label');
		lLabel.setAttribute('class', 'white-text');
		lLabel.setAttribute('id', 'lLabel');
		lLabel.appendChild(document.createTextNode('Left text'));
		var rLabel = document.createElement('label');
		rLabel.setAttribute('class', 'white-text');
		rLabel.setAttribute('id', 'rLabel');
		rLabel.appendChild(document.createTextNode('Right text'));

		var leftInput = document.createElement('input');
		leftInput.setAttribute('type', 'text');
		leftInput.setAttribute('id', 'leftInput');

		var rightInput = document.createElement('input');
		rightInput.setAttribute('type', 'text');
		rightInput.setAttribute('id', 'rightInput');

		d.appendChild(textLabel);
		d.appendChild(lLabel);
		d.appendChild(leftInput);
		d.appendChild(rLabel);
		d.appendChild(rightInput);
	}


		var nextButton = document.createElement('button');
		nextButton.setAttribute('id', 'nextButton');
		nextButton.setAttribute('onclick', 'nextButton()');
		nextButton.appendChild(document.createTextNode('Next image'));
		d.appendChild(nextButton);
		document.getElementById('controls').appendChild(d);

	// start time measurement for readability test
	t0 = performance.now();
}

function nextButton()
{
	// stop time measurement for readability time
	t1 = performance.now();
	readabilityTime = t1 - t0;

	// start new scene
	loadJSON(function(response)
	{
		// parse json into object
		var s = JSON.parse(response);

		// answered scene
		var currentScene = s[(model - 1) * 4 + sequence[model - 1][currentPosition]];

		if (currentPosition == 3)
		{
			var defaultLeft = currentScene.leftText.toLowerCase();
			var userLeft = document.getElementById('leftInput').value.toLowerCase();
			var defaultRight = currentScene.rightText.toLowerCase();
			var userRight = document.getElementById('rightInput').value.toLowerCase();

			var error = 1;
			if (defaultLeft == userLeft)
				error = 0;

			var data = [];
			data.push({	name: 'text-error', value: error});
			data.push({	name: 'text-model', value: sceneNames[model - 1]});
			data.push({	name: 'text-method', value: method - 1});
			Transfer.sendData('testapp2-text', data);
			//textTestData.push(data);

			data = [];
			error = 1;
			if (defaultRight == userRight)
				error = 0;

			data.push({	name: 'text-error', value: error});
			data.push({	name: 'text-model', value: sceneNames[model - 1]});
			data.push({	name: 'text-method', value: method - 1});
			Transfer.sendData('testapp2-text', data);
			//textTestData.push(data);
		}

		currentPosition++;

		if (currentPosition > 3)
		{
			currentSceneCnt++;
			/*if (currentSceneCnt == 3)
			{
				sendData(depthTestData, partToken);
				sendData(textTestData, 'testapp2-text');
			}*/
			updateProperties();
			redirectToNextModel('.');
			currentPosition = 0;
			setTimeout(function setGUI()
			{
				document.getElementById(currentScene.model + '_' + currentScene.methods[method - 1]).style.display = "none";
				var d = document.getElementById('buttons');
				d.outerHTML = '';
				document.getElementById('rightText').disabled = false;
				document.getElementById('leftText').disabled = false;
				document.getElementById('noText').disabled = false;
				var scene = s[(model - 1) * 4 + sequence[model - 1][currentPosition]];
				document.getElementById(scene.model + '_' + scene.methods[method - 1]).style.display = '';
			}, 1000);
		}
		else
		{
			document.getElementById(currentScene.model + '_' + currentScene.methods[method - 1]).style.display = "none";
			var d = document.getElementById('buttons');
			d.outerHTML = '';
			document.getElementById('rightText').disabled = false;
			document.getElementById('leftText').disabled = false;
			document.getElementById('noText').disabled = false;
			var scene = s[(model - 1) * 4 + sequence[model - 1][currentPosition]];
			document.getElementById(scene.model + '_' + scene.methods[method - 1]).style.display = '';
		}

	}, 'depth.json');
}
