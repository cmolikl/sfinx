function createGaugeCanvas()
{
	var width = 120;
	var height = 120;
	var scene = new THREE.Scene();

	var camera = new THREE.OrthographicCamera();
	camera.left = width / -2;
	camera.right = width / 2;
	camera.top = height/ 2;
	camera.bottom = height / -2;
	camera.near = 0.1;
	camera.far = 1500;
	camera.updateProjectionMatrix();
	camera.position.set(0, 0, 500);
	camera.lookAt(scene.position);

	var renderer = new THREE.WebGLRenderer( {antialias: true});
	renderer.setSize( width, height );

	var light = new THREE.DirectionalLight( 0xffffff );	
	light.position.set( 0, 0, 1 );
	scene.add( light );

	document.getElementById('gaugeCanvas').appendChild( renderer.domElement );

	var geometry = new THREE.CylinderGeometry( 4, 4, 46, 12 );
	var material = new THREE.MeshPhongMaterial( {color: 0xff0000} );
	var cylinder = new THREE.Mesh( geometry, material );

	var geometry = new THREE.CylinderGeometry( 35, 35, 4, 35 );
	var material = new THREE.MeshPhongMaterial( { color: 0xff0000 } );
	var torus = new THREE.Mesh( geometry, material );

	torus.position.y = -23;

	pivotPoint = new THREE.Object3D();
	pivotPoint.position.set(0,23,0);
	pivotPoint.add(cylinder);
	pivotPoint.add(torus);

	var group = new THREE.Group();
	group.add( pivotPoint );

	scene.add( group );


	var animate = function () 
	{
		requestAnimationFrame( animate );
		renderer.render(scene, camera);
	};

	animate();

	return group;
}