var testSequence = ['shapeDescription', 'shape', 'depthDescription', 'depth'];
var testsCount = 4;
var modelSequences = [[1, 2, 4, 3],
                       [2, 3, 1, 4],
                       [3, 4, 2, 1],
                       [4, 1, 3, 2]];
var modelsCount = [1, 4, 1, 4];
var methodsCount = 4;
var timeout = 1000;


function changeImg() {
    const img = document.getElementById("target-img");
    const id = sequence[index++];
    if (id != undefined) {
        img.src = `image${id}.png`;
        timerStart();
    } else {
        // the end of test
        img.src = "image.png";
        endTest();
    }
}

function labelClick(label_no) {
    var completionTime = timerStop();
    var error;
    if (label_no < 0) {
        error = Math.abs(label_no);
    } else {
        error = label_no == sequence[index - 1] ? 0 : 1;
    }

    // send data
    // time + error + model
    var data = [];
    data.push({ name: 'Model', value: model });
    data.push({ name: 'Time',  value: completionTime });
    data.push({ name: 'Error', value: error });

    Transfer.sendData(partToken, data);
    // change img
    setTimeout(changeImg, timeout);
}

function startTest() {
    var counter = localStorage.getItem("counter");
    if (counter != undefined) {
        localStorage.setItem("counter", Number(counter)+1);
    } else {
        // first time
        localStorage.setItem("counter", 1);
    }

    document.getElementById("start").disabled = true;
    document.getElementById("cannotDecide").disabled = false;
    document.getElementById("noLabel").disabled = false;
    setTimeout(changeImg, timeout);
}

function endTest() {
    document.getElementById("cannotDecide").disabled = true;
    document.getElementById("noLabel").disabled = true;
    document.getElementById("start").disabled = true;

    $("#img-container").remove();

    var counter = Number(localStorage.getItem("counter"));
    var model_index = counter % model_sequence.length;

    if (model_index == 0) {
        window.location.replace("../finish.html");
    } else {
        document.write('Redirecting...');
        redirectToNextModel('../');
    }
}

function redirectToNextModel(path) {
    setTimeout(function() {
        //console.info('Participant: ', participantIndex);
        //console.info('Redirecting to test: ', `${path}/${test}_${method}_${model}/index.html`);

        if(testIndex >= testsCount) {
          window.location.replace(`${path}/finish.html`);
        }
        else {
          window.location.replace(`${path}/${test}.html`);
        }
    }, timeout);
}

function initProperties() {
  participantIndex =  Number(localStorage.getItem('participantIndex'));
  testIndex = localStorage.getItem('testIndex');
  modelIndex = localStorage.getItem('modelIndex');
}

function updateProperties() {
  initProperties();

  console.info('testIndex', testIndex);
  console.info('modelIndex', modelIndex);

  if(testIndex == undefined) {
    console.info('testIndex undefined');
    testIndex = 0;
    modelIndex = 0;
  }
  else if(modelIndex == modelsCount[testIndex]-1) {
      // All models of test done, redirect to next test
      testIndex++;
      modelIndex = 0;
  }
  else {
    // Redirect to next model of the current test
    modelIndex++;
  }

  localStorage.setItem('testIndex', testIndex);
  localStorage.setItem('modelIndex', modelIndex);

  test = testSequence[testIndex];

  if(testIndex >= testsCount-1) {
    // All test are done return
    return;
  }

  counterbalanceModel(participantIndex, modelIndex);initProperties();
}

function counterbalanceModel(participantIndex, modelIndex) {
  // Counterbalancing based on participantIndex
  var sequenceIndex = participantIndex % modelsCount[testIndex];
  model = modelSequences[sequenceIndex][modelIndex];
  method = (Math.floor(participantIndex / methodsCount) % methodsCount) + 1;
}

function timerStart() {
    startTime = window.performance.now();
}

function timerStop() {
    return window.performance.now() - startTime;
}
