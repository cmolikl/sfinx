
var currentSceneCnt = 0;
var currentPosition = 0;
var userAnswer;

var sequence;
var modelSeq, method;
var t0, t1;

var depthTime, readabilityTime;
var numOfScenes;

var sequence;

var testDepth = function ()
{
	var container = document.getElementById('imgContainer');
	var imgNode = document.createElement('img');
	imgNode.src = 'img/test2.png';
	imgNode.id = 'imgtest2';
	imgNode.style.width = '800px';
	imgNode.style.height = '800px';

	container.appendChild(imgNode);

}

function getText(input)
{
	// choose answer
	if (input == 1)
	{
		// left text is closer
		var right = document.getElementById('rightText');
		right.disabled = true;
		var no = document.getElementById('noText');
		no.disabled = true;
		userAnswer = 'L';
	}
	else if (input == 2)
	{
		// right text is closer
		var left = document.getElementById('leftText');
		left.disabled = true;
		var no = document.getElementById('noText');
		no.disabled = true;
		userAnswer = 'R';
	}
	else if (input == 0)
	{
		var left = document.getElementById('leftText');
		left.disabled = true;
		var right = document.getElementById('rightText');
		right.disabled = true;
		userAnswer = 'N';
	}

	// create input for texts
	var d = document.getElementById('buttons');
	if (d != null)
		d.outerHTML = '';
	d = document.createElement('div');
	d.setAttribute('id', 'buttons');

	var textLabel = document.createElement('label');
	textLabel.setAttribute('id', 'readabilityLabel');
	textLabel.setAttribute('class', 'white-text');
	textLabel.appendChild(document.createTextNode('Rewrite texts from image'));
	var lLabel = document.createElement('label');
	lLabel.setAttribute('class', 'white-text');
	lLabel.setAttribute('id', 'lLabel');
	lLabel.appendChild(document.createTextNode('Left text'));
	var rLabel = document.createElement('label');
	rLabel.setAttribute('class', 'white-text');
	rLabel.setAttribute('id', 'rLabel');
	rLabel.appendChild(document.createTextNode('Right text'));

	var leftInput = document.createElement('input');
	leftInput.setAttribute('type', 'text');
	leftInput.setAttribute('id', 'leftInput');

	var rightInput = document.createElement('input');
	rightInput.setAttribute('type', 'text');
	rightInput.setAttribute('id', 'rightInput');

	var nextButton = document.createElement('button');
	nextButton.setAttribute('id', 'nextButton');
	nextButton.setAttribute('onclick', 'nextButton()');
	nextButton.appendChild(document.createTextNode('Next image'));

	d.appendChild(textLabel);
	d.appendChild(lLabel);
	d.appendChild(leftInput);
	d.appendChild(rLabel);
	d.appendChild(rightInput);
	d.appendChild(nextButton);

	document.getElementById('controls').appendChild(d);
}

function nextButton()
{
	window.location.replace('depthDescription.html');
}
