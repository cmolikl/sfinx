var gauge, bigGauge;
var view;
var sequence, pointSequence;
var currentSceneCnt = 0;
var currentScenePoint = 0;
var eps = 0.01;
var numOfScenes;
var camera;
var scene, renderer;
var t0, t1;

// data to send to stat app
var gaugeFigureTime, readabilityTime;

// ------------   SKELET FUNCTION   ------------
var testSkelet = function ()
{
	var container = document.getElementById('imgContainer');
	var imgNode = document.createElement('img');
	imgNode.src = 'img/test.png';
	imgNode.id = 'imgTest'
	imgNode.style.width = '800px';
	imgNode.style.height = '800px';

	container.appendChild(imgNode);
	// draw gauge to canvas
	bigGauge = createGaugeCanvas();

	// define width and height
	var width = 800;
	var height = 800;

	//create scene
	scene = new THREE.Scene();

	// set camera
	camera = new THREE.OrthographicCamera();
	camera.left = width / -2;
	camera.right = width / 2;
	camera.top = height/ 2;
	camera.bottom = height / -2;
	camera.near = 0.1;
	camera.far = 50;
	camera.updateProjectionMatrix();
	camera.position.set(0, 0, 20);
	camera.up = new THREE.Vector3(0, 1, 0);
	camera.lookAt(scene.position);

	// init renderer
	renderer = new THREE.WebGLRenderer({ alpha: true , antialias: true});
	renderer.setSize(800, 800);

	// add rendener to canvas
	c.appendChild( renderer.domElement );

	// add main light (from camera)
	addLight(scene);

	// display gauge in scene
	gauge = drawGauge(scene);

	// display current scene
	displayCurrentScene();
}

// ------------   SCENE HANDLER   ------------
var enabled = true;

function displayCurrentScene()
{
	// choose current scene for testing
	var sceneImage = document.getElementById('imgTest');
	var userNormal = new THREE.Vector3(0, 1, 0);

	sceneImage.style.display = "";
	gauge.position.set(0, 0, 0);
	gauge.rotation.set(0, 0, 0);
	bigGauge.rotation.set(0, 0, 0);
	view = (scene.position.sub(camera.position)).normalize();

	// rotate gauge
	var isDragging = false;
	var previousMousePosition = {
	    x: 0,
	    y: 0
	};

	// mouse pressed and moving
	$(renderer.domElement).on('mousedown', function(e) { isDragging = true; }).on('mousemove', function(e)
	{
	   	var deltaMove = {
	       	x: e.offsetX - previousMousePosition.x,
	       	y: e.offsetY - previousMousePosition.y
	   	};
	    if(isDragging && enabled)
	    {
	    	var normal = getGaugeNormal(gauge);
	    	var dotNV = normal.dot(view);
	    	if (dotNV <= 0)
	    	{
				gauge.rotateOnWorldAxis(new THREE.Vector3(1,0,0), toRadians(deltaMove.y));
				bigGauge.rotateOnWorldAxis(new THREE.Vector3(1,0,0), toRadians(deltaMove.y));
				gauge.rotateOnWorldAxis(new THREE.Vector3(0,1,0), toRadians(deltaMove.x));
				bigGauge.rotateOnWorldAxis(new THREE.Vector3(0,1,0), toRadians(deltaMove.x));
			}
			else
			{
				if (normal.y >= 0)
				{
					gauge.rotateOnWorldAxis(new THREE.Vector3(1,0,0), toRadians(0.5));
					bigGauge.rotateOnWorldAxis(new THREE.Vector3(1,0,0), toRadians(0.5));
				}
				else if (normal.y < 0)
				{
					gauge.rotateOnWorldAxis(new THREE.Vector3(1,0,0), toRadians(-0.5));
					bigGauge.rotateOnWorldAxis(new THREE.Vector3(1,0,0), toRadians(-0.5));
				}
			}
	    }

	    previousMousePosition = {
	        x: e.offsetX,
	        y: e.offsetY
	    };

	});

	// mouse released
	$(document).on('mouseup', function(e) {
	    isDragging = false;
	});

	var animate = function ()
	{
		requestAnimationFrame( animate );
		renderer.render(scene, camera);
	};

	animate();
}

// ------------   GAUGE FUNCTIONS   ------------
function drawGauge(scene)
{
	var geometry = new THREE.CylinderGeometry( 1, 1, 20, 12 );
	var material = new THREE.MeshPhongMaterial( {color: 0xff0000} );
	var cylinder = new THREE.Mesh( geometry, material );

	var geometry = new THREE.TorusGeometry( 15, 1, 20, 35 );
	var material = new THREE.MeshPhongMaterial( { color: 0xff0000 } );
	var torus = new THREE.Mesh( geometry, material );

	torus.rotation.x = Math.PI / 2;
	torus.position.y = -10;

	auxPoint = new THREE.Object3D();
	auxPoint.position.set(0,10,0);
	auxPoint.name = 'auxPoint';

	pivotPoint = new THREE.Object3D();
	pivotPoint.position.set(0,10,0);
	pivotPoint.name = 'pivotPoint'
	pivotPoint.add(auxPoint);
	pivotPoint.add(cylinder);
	pivotPoint.add(torus);

	var group = new THREE.Group();
	group.add( pivotPoint );

	scene.add( group );

	return group;
}

function getGaugeNormal()
{
	var point = new THREE.Vector3();
	var pivot = new THREE.Vector3();
	gauge.children[0].getObjectByName('auxPoint').getWorldPosition(point);
	gauge.getObjectByName('pivotPoint').getWorldPosition(pivot);
	return point.sub(pivot).normalize();
}

// ------------   OTHER FUNCTIONS   ------------
function addLight(scene)
{
	var light = new THREE.DirectionalLight( 0xffffff );
	light.position.set( 0, 0, 5 );
	scene.add( light );
}

function toRadians(angle)
{
	return angle * (Math.PI / 180);
}

function toDegrees(angle)
{
	return angle * (180 / Math.PI);
}


// ------------   BUTTON CONTROLS   ------------
function upButton()
{
	var normal = getGaugeNormal();
	var dotNV = normal.dot(view);
	var axis = new THREE.Vector3(0, 1, 0);
	axis = axis.cross(normal);
	if (dotNV < 0 || (Math.abs(dotNV) < eps && normal.y < 0))
	{
		gauge.rotateOnWorldAxis(axis, toRadians(-1));
		bigGauge.rotateOnWorldAxis(axis, toRadians(-1));
	}
}

function downButton()
{
	var normal = getGaugeNormal();
	var dotNV = normal.dot(view);
	var axis = new THREE.Vector3(0, 1, 0);
	axis = axis.cross(normal);
	if (dotNV < 0 || (Math.abs(dotNV) < eps && normal.y > 0))
	{
		gauge.rotateOnWorldAxis(axis, toRadians(1));
		bigGauge.rotateOnWorldAxis(axis, toRadians(1));
	}
}

function leftButton()
{
	var normal = getGaugeNormal();
	var dotNV = normal.dot(view);
	var axis = new THREE.Vector3(-1, 0, 0);
	axis = axis.cross(normal);
	if (dotNV < 0 || (Math.abs(dotNV) < eps && normal.x > 0))
	{
		gauge.rotateOnWorldAxis(axis, toRadians(-1));
		bigGauge.rotateOnWorldAxis(axis, toRadians(-1));
	}
}

function rightButton()
{
	var normal = getGaugeNormal();
	var dotNV = normal.dot(view);
	var axis = new THREE.Vector3(-1, 0, 0);
	axis = axis.cross(normal);
	if (dotNV < 0 || (Math.abs(dotNV) < eps && normal.x < 0))
	{
		gauge.rotateOnWorldAxis(axis, toRadians(1));
		bigGauge.rotateOnWorldAxis(axis, toRadians(1));
	}
}

function gaugeSet()
{
	window.location.replace('shapeDescription.html');
}

function resetButton()
{
	gauge.rotation.set(0, 0, 0);
	bigGauge.rotation.set(0, 0, 0);
}
