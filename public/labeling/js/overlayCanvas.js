var gauge, bigGauge;
var view;
var sequence, pointSequence;
var currentSceneCnt = 0;
var currentScenePoint = 0;
var eps = 0.01;
var numOfScenes;
var camera;
var scene, renderer;
var t0, t1;

// balanced latin square for tests
var models = [
	[1, 2, 4, 3],
	[2, 3, 1, 4],
	[3, 4, 2, 1],
	[4, 1, 3, 2]
];

// data to send to stat app
var gaugeFigureTime, readabilityTime;

var shapeTestData = [];
var textTestData = [];

// ------------   SKELET FUNCTION   ------------
var skelet = function ()
{
	initProperties();
	counterbalanceModel(participantIndex, modelIndex);
	console.log('model: ' + model + ' method: ' + method);

	loadJSON(function(response)
	{
		var s = JSON.parse(response);
		var container = document.getElementById('imgContainer');
		numOfScenes = s.length;

		// loading all images for test
		for(var i = 0; i < s.length; i++)
		{
			for (var j = 0; j < s[i].methods.length; j++)
			{
				var imgNode = document.createElement('img');
				imgNode.src = 'img/' + s[i].model + '/' + s[i].methods[j] + '.png';
				imgNode.id = s[i].model + '_' + s[i].methods[j];
				imgNode.style.width = '800px';
				imgNode.style.height = '800px';
				imgNode.style.display = 'none';

				container.appendChild(imgNode);
			}

			var normalNode = document.createElement('img');
			normalNode.src = 'img/' + s[i].model + "/" + s[i].normalMap + '.png';
			normalNode.id = s[i].model + '_' + s[i].normalMap;
			normalNode.style.width = '800px';
			normalNode.style.height = '800px';
			normalNode.style.display = 'none';

			container.appendChild(normalNode);
		}

		// generate random point sequence
		pointSequence = new Array(numOfScenes);

		for (var i = 0; i < numOfScenes; i++)
		{
			var numOfPoints = s[i].gaugePosition.length;
			pointSequence[i] = randomSequence(numOfPoints);
		}

		// draw gauge to canvas
		bigGauge = createGaugeCanvas();

		// define width and height
		var width = 800;
		var height = 800;

		//create scene
		scene = new THREE.Scene();

		// set camera
		camera = new THREE.OrthographicCamera();
		camera.left = width / -2;
	    camera.right = width / 2;
	    camera.top = height/ 2;
	    camera.bottom = height / -2;
	    camera.near = 0.1;
	    camera.far = 50;
	    camera.updateProjectionMatrix();
	    camera.position.set(0, 0, 20);
	   	camera.up = new THREE.Vector3(0, 1, 0);
	    camera.lookAt(scene.position);

	    // init renderer
		renderer = new THREE.WebGLRenderer({ alpha: true , antialias: true});
		renderer.setSize(800, 800);

		// add rendener to canvas
		c.appendChild( renderer.domElement );

		// add main light (from camera)
		addLight(scene);

		// display gauge in scene
		gauge = drawGauge(scene);

		// display current scene
		displayCurrentScene(s[model - 1]);

	}, 'scenes.json');
}

// ------------   SCENE HANDLER   ------------
var enabled = true;

function displayCurrentScene(currentScene)
{
	// choose current scene for testing
	var sceneImage = document.getElementById(currentScene.model + '_' + currentScene.methods[method - 1]);
	var userNormal = new THREE.Vector3(0, 1, 0);
	var positions = currentScene.gaugePosition;

	sceneImage.style.display = "";
	gauge.position.set(positions[currentScenePoint].x, positions[currentScenePoint].y, 0);
	gauge.rotation.set(0, 0, 0);
	bigGauge.rotation.set(0, 0, 0);
	view = (scene.position.sub(camera.position)).normalize();

	// start time measurement for gauge figure task
	t0 = performance.now();

	// rotate gauge
	var isDragging = false;
	var previousMousePosition = {
	    x: 0,
	    y: 0
	};

	// mouse pressed and moving
	$(renderer.domElement).on('mousedown', function(e) { isDragging = true; }).on('mousemove', function(e)
	{
	   	var deltaMove = {
	       	x: e.offsetX - previousMousePosition.x,
	       	y: e.offsetY - previousMousePosition.y
	   	};
	    if(isDragging && enabled)
	    {
	    	var normal = getGaugeNormal(gauge);
	    	var dotNV = normal.dot(view);
	    	if (dotNV <= 0)
	    	{
				gauge.rotateOnWorldAxis(new THREE.Vector3(1,0,0), toRadians(deltaMove.y));
				bigGauge.rotateOnWorldAxis(new THREE.Vector3(1,0,0), toRadians(deltaMove.y));
				gauge.rotateOnWorldAxis(new THREE.Vector3(0,1,0), toRadians(deltaMove.x));
				bigGauge.rotateOnWorldAxis(new THREE.Vector3(0,1,0), toRadians(deltaMove.x));
			}
			else
			{
				if (normal.y >= 0)
				{
					gauge.rotateOnWorldAxis(new THREE.Vector3(1,0,0), toRadians(0.5));
					bigGauge.rotateOnWorldAxis(new THREE.Vector3(1,0,0), toRadians(0.5));
				}
				else if (normal.y < 0)
				{
					gauge.rotateOnWorldAxis(new THREE.Vector3(1,0,0), toRadians(-0.5));
					bigGauge.rotateOnWorldAxis(new THREE.Vector3(1,0,0), toRadians(-0.5));
				}
			}
	    }

	    previousMousePosition = {
	        x: e.offsetX,
	        y: e.offsetY
	    };

	});

	// mouse released
	$(document).on('mouseup', function(e) {
	    isDragging = false;
	});

	var animate = function ()
	{
		requestAnimationFrame( animate );
		renderer.render(scene, camera);
	};

	animate();
}

// ------------   GAUGE FUNCTIONS   ------------
function drawGauge(scene)
{
	var geometry = new THREE.CylinderGeometry( 1, 1, 20, 12 );
	var material = new THREE.MeshPhongMaterial( {color: 0xff0000} );
	var cylinder = new THREE.Mesh( geometry, material );

	var geometry = new THREE.TorusGeometry( 15, 1, 20, 35 );
	var material = new THREE.MeshPhongMaterial( { color: 0xff0000 } );
	var torus = new THREE.Mesh( geometry, material );

	torus.rotation.x = Math.PI / 2;
	torus.position.y = -10;

	auxPoint = new THREE.Object3D();
	auxPoint.position.set(0,10,0);
	auxPoint.name = 'auxPoint';

	pivotPoint = new THREE.Object3D();
	pivotPoint.position.set(0,10,0);
	pivotPoint.name = 'pivotPoint'
	pivotPoint.add(auxPoint);
	pivotPoint.add(cylinder);
	pivotPoint.add(torus);

	var group = new THREE.Group();
	group.add( pivotPoint );

	scene.add( group );

	return group;
}

function getGaugeNormal()
{
	var point = new THREE.Vector3();
	var pivot = new THREE.Vector3();
	gauge.children[0].getObjectByName('auxPoint').getWorldPosition(point);
	gauge.getObjectByName('pivotPoint').getWorldPosition(pivot);
	return point.sub(pivot).normalize();
}

// ------------   OTHER FUNCTIONS   ------------
function addLight(scene)
{
	var light = new THREE.DirectionalLight( 0xffffff );
	light.position.set( 0, 0, 5 );
	scene.add( light );
}

function toRadians(angle)
{
	return angle * (Math.PI / 180);
}

function toDegrees(angle)
{
	return angle * (180 / Math.PI);
}


// ------------   BUTTON CONTROLS   ------------
function upButton()
{
	var normal = getGaugeNormal();
	var dotNV = normal.dot(view);
	var axis = new THREE.Vector3(0, 1, 0);
	axis = axis.cross(normal);
	if (dotNV < 0 || (Math.abs(dotNV) < eps && normal.y < 0))
	{
		gauge.rotateOnWorldAxis(axis, toRadians(-1));
		bigGauge.rotateOnWorldAxis(axis, toRadians(-1));
	}
}

function downButton()
{
	var normal = getGaugeNormal();
	var dotNV = normal.dot(view);
	var axis = new THREE.Vector3(0, 1, 0);
	axis = axis.cross(normal);
	if (dotNV < 0 || (Math.abs(dotNV) < eps && normal.y > 0))
	{
		gauge.rotateOnWorldAxis(axis, toRadians(1));
		bigGauge.rotateOnWorldAxis(axis, toRadians(1));
	}
}

function leftButton()
{
	var normal = getGaugeNormal();
	var dotNV = normal.dot(view);
	var axis = new THREE.Vector3(-1, 0, 0);
	axis = axis.cross(normal);
	if (dotNV < 0 || (Math.abs(dotNV) < eps && normal.x > 0))
	{
		gauge.rotateOnWorldAxis(axis, toRadians(-1));
		bigGauge.rotateOnWorldAxis(axis, toRadians(-1));
	}
}

function rightButton()
{
	var normal = getGaugeNormal();
	var dotNV = normal.dot(view);
	var axis = new THREE.Vector3(-1, 0, 0);
	axis = axis.cross(normal);
	if (dotNV < 0 || (Math.abs(dotNV) < eps && normal.x < 0))
	{
		gauge.rotateOnWorldAxis(axis, toRadians(1));
		bigGauge.rotateOnWorldAxis(axis, toRadians(1));
	}
}

function gaugeSet()
{
	// stop measuring the time for gauge figure task
	t1 = performance.now();
	gaugeFigureTime = t1 - t0;

	loadJSON(function(response){

		var s = JSON.parse(response);
		var currentScene = s[model - 1];

		// get gauge data from image
		var normal = getGaugeNormal();
		var realNormal = readNormal(currentScene);
		var realNormalTilt = Math.atan2(realNormal.y,realNormal.x) * 180 / Math.PI;
		var userNormalTilt = Math.atan2(normal.y,normal.x) * 180 / Math.PI;

		if (realNormalTilt < 0)
			realNormalTilt += 360;

		if (userNormalTilt < 0)
			userNormalTilt += 360;

		var realNormalSlant = 180/Math.PI * Math.acos(realNormal.z);
		var userNormalSlant = 180/Math.PI * Math.acos(normal.z);

		var tiltError = realNormalTilt - userNormalTilt;
		var slantError = realNormalSlant - userNormalSlant;

		var absTiltError = Math.abs(tiltError);
		var absSlantError = Math.abs(slantError);
		var error = Math.acos(normal.x*realNormal.x + normal.y*realNormal.y + normal.z*realNormal.z) * 180 / Math.PI;

		var data = [];
		data.push({name: 'shape-time', value: gaugeFigureTime});
		data.push({name: 'shape-tiltError', value: tiltError});
		data.push({name: 'shape-slantError', value: slantError});
		data.push({name: 'shape-absTiltError', value: absTiltError});
		data.push({name: 'shape-absSlantError', value: absSlantError});
		data.push({name: 'shape-error', value: error});
		data.push({name: 'shape-model', value: sceneNames[model-1]});
		data.push({name: 'shape-method', value: (method - 1).toString()});

		Transfer.sendData(partToken, data);
		//shapeTestData.push(data);

	    // create new input field with button
		if (currentScenePoint == 3)
		{
			var d = document.getElementById('buttons');
			d.outerHTML = '';
			d = document.createElement('div');
			d.setAttribute('id', 'buttons');

			var textLabel = document.createElement('label');
			textLabel.setAttribute('id', 'readabilityLabel');
			textLabel.setAttribute('class', 'white-text');
			textLabel.setAttribute('for', 'readabilityInput');
			textLabel.appendChild(document.createTextNode('Rewrite text from image'));

			var textInput = document.createElement('input');
			textInput.setAttribute('type', 'text');
			textInput.setAttribute('id', 'readabilityInput');

			var nextButton = document.createElement('button');
			nextButton.setAttribute('id', 'nextButton');
			nextButton.setAttribute('onclick', 'nextButton()');
			nextButton.appendChild(document.createTextNode('Next image'));

			d.appendChild(textLabel);
			d.appendChild(textInput);
			d.appendChild(nextButton);
			enabled = false;

			document.getElementById('controls').appendChild(d);
			t0 = performance.now();
		}
		else
		{
			currentScenePoint++;
			displayCurrentScene(currentScene);
		}

	}, 'scenes.json');
}

function nextButton()
{
	// stop measuring the time for readability task
	t1 = performance.now();
	readabilityTime = t1 - t0;

	loadJSON(function(response)
	{
		// parse json into object
		var s = JSON.parse(response);
		var currentScene = s[model - 1];
		var sceneImage = document.getElementById(currentScene.model + '_' + currentScene.methods[method - 1]);
		sceneImage.style.display = "none";

		// get text
		var defaultText = currentScene.leftText.toLowerCase();
		var userText = document.getElementById('readabilityInput').value.toLowerCase();

		var error = 1;
		if (defaultText == userText)
			error = 0;

		// push result
		var data = [];
		data.push({name: 'text-error', value: error});
		data.push({name: 'text-model', value: sceneNames[model - 1]});
		data.push({name: 'text-method', value: method - 1});

		Transfer.sendData('testapp2-text', data);
		//textTestData.push(data);

		// create buttons
		setTimeout(function setGUI()
		{
			var d = document.getElementById('buttons');
			d.outerHTML = '';
			d = document.createElement('div');
			d.setAttribute('id', 'buttons');

			var resetButton = document.createElement('button');
			resetButton.setAttribute('id', 'resetButton');
			resetButton.setAttribute('onclick', 'resetButton()');
			resetButton.appendChild(document.createTextNode('Reset gauge'));

			var gaugeButton = document.createElement('button');
			gaugeButton.setAttribute('id', 'gaugeSetButton');
			gaugeButton.setAttribute('onclick', 'gaugeSet()');
			gaugeButton.appendChild(document.createTextNode('Set gauge'));

			d.appendChild(resetButton);
			d.appendChild(gaugeButton);
			enabled = true;

			document.getElementById('controls').appendChild(d);
		}, 2000);


		// change to new scene
		currentSceneCnt++;
		currentScenePoint = 0;

		/*if (currentSceneCnt == 3)
		{
			sendData(shapeTestData, partToken);
			sendData(textTestData, 'testapp2-text');
		}*/

		updateProperties();
		redirectToNextModel('.');

		// display current scene
		displayCurrentScene(s[model - 1]);

	}, 'scenes.json');
}

function resetButton()
{
	gauge.rotation.set(0, 0, 0);
	bigGauge.rotation.set(0, 0, 0);
}

function readNormal(currentScene)
{
	var normal = document.getElementById(currentScene.model + '_' + currentScene.normalMap);
	var canvas = document.createElement('canvas');
	canvas.setAttribute('id', 'c');
	canvas.width = normal.width;
	canvas.height = normal.height;
	canvas.getContext('2d').drawImage(normal, 0, 0, normal.width, normal.height);

	var imageNormal = canvas.getContext('2d').getImageData(gauge.position.x + 400, 400 - gauge.position.y, 1, 1).data;

	var iteratorArray = imageNormal[Symbol.iterator]();
	var x = iteratorArray.next().value / 255 * 2 - 1;
	var y = iteratorArray.next().value / 255 * 2 - 1;
	var z = iteratorArray.next().value / 255 * 2 - 1;

	var a = new THREE.Vector3(x,y,z);
	a.normalize();
	return a;
}
