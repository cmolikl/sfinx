function loadJSON(callback, filename) 
{   
	// load JSON file with scene definitions
	var xobj = new XMLHttpRequest();
	xobj.overrideMimeType("application/json");
	xobj.open('GET', filename, true);
	xobj.onreadystatechange = function () {
		if (xobj.readyState == 4 && xobj.status == "200") 
		{
			return callback(xobj.responseText);
		}
    };
    xobj.send(null);
 }

function randomSequence(N)
{
	var arr = [...Array(N).keys()];
	arr = shuffle(arr);
	return arr;
}

function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}