class Participant < ApplicationRecord
  belongs_to :experiment
  acts_as_sequenced scope: :experiment_id
  has_many :data, class_name: 'Experiment::Datum', dependent: :destroy
  has_many :json_data, class_name: 'Experiment::JsonDatum', dependent: :destroy

end
