class Api::V1::ExperimentDataController < ActionController::API
  respond_to :json
  before_action :ensure_json_request
  # before_action :set_cors_headers
  before_action :check_experiment_param, :find_experiment, :check_experiment_state, only: :register_participant
  before_action :find_part, :check_params, :find_participant, :check_experiment_state, :check_repetition_count, only: :create


  def register_participant

    participant_data = {
      experiment_id: @experiment.id,
      internal_id: SecureRandom.uuid,
      finished_parts: 0
    }

    # add external_id to data if it's supplied
    participant_data[:external_id] = params[:external_id] if params[:external_id]

    timeout_time = Time.current() - @experiment.timeout*60 
    # Before creating participant we need to find oldest timeouted participant that didn't finished all parts
    # If we get him, reuse his sequential_id (these two actions need to be atomic)
    ActiveRecord::Base.transaction do
      ActiveRecord::Base.connection.execute('LOCK TABLE participants IN EXCLUSIVE MODE')
      timeouted_participant = Participant.where("created_at < ? AND finished_parts < ?", timeout_time, @experiment.number_of_parts).order(created_at: :asc).first
      if timeouted_participant
        participant_data[:sequential_id] = timeouted_participant.sequential_id
        timeouted_participant.destroy
      end
      participant = Participant.create(participant_data)
      render json: { internal_id: participant.internal_id, sequential_id: participant.sequential_id }
    end
  end

  def create
    # [ {name: "age", value: "77"}, ..., ... ]
    flat_json = {}

    variable_params[:variable_values].each do |variable_row|
      variable = find_variable_by(variable_row["name"]) or return

      recordClass = "#{variable.type}_datum".classify.constantize
      record = recordClass.create(value: variable_row[:value])

      datum = Experiment::Datum.create(
        target_id: record.id,
        target_type: recordClass,
        participant_id: @participant.id,
        variable_id: variable.id
      )

      flat_json[variable_row[:name]] = variable_row[:value]
    end

    # Save to json table
    json_datum = Experiment::JsonDatum.create(
      experiment_id: @experiment.id,
      part_id: @current_part.id,
      participant_id: @participant.id,
      data: flat_json
    )

    #Check if participant finished current part of experiment
    #If yes increase the # of finished parts and update participant data
    if @existing_data.size == @current_part.repetition_count
      @participant.finished_parts += 1
      @participant.save()
    end

    send_json_status('Ok', 200)
  end

  # def cors_preflight
  #   render(nothing: :true) and return
  # end

  private

  ################ Before registering participant ###################

  def check_experiment_param
    # check existance of participant
    if !params[:experiment_id]
      return send_json_status('experiment_id is missing', 422)
    end
  end

  def find_experiment
    @experiment = Experiment.find_by!(access_token: params[:experiment_id])
  rescue ActiveRecord::RecordNotFound
    return send_json_status("Experiment not found", 404)
  end

  def check_experiment_state
    if @experiment.edit? || @experiment.closed?
      return send_json_status('Experiment is currently unavailable', 403)
    end
  end

  ################ Before storing data ###################

  def check_params
    # check existance of participant
    if !params[:internal_id] && !params[:external_id]
      return send_json_status('internal_id or external_id are missing', 422)
    end
    # check existance of values
    if !params[:variable_values]
      return send_json_status('variable_values are missing', 422)
    end
    # check type (must be Hash structure)
    unless params[:variable_values].is_a? Array
      return send_json_status('variable_values must be an Array', 422)
    end
    # check Hash size
    if params[:variable_values].size != @current_part.variables.count
      return send_json_status('Size of variable_values is not correct', 422)
    end
  end

  def find_participant
    find_options = if (params[:external_id])
                    { external_id: params[:external_id] }
                   else
                    { internal_id: params[:internal_id] }
                   end
    @participant = Participant.find_by!(find_options)
  rescue ActiveRecord::RecordNotFound
    return send_json_status('Participant not found', 404)
  end

  def find_part
    @current_part = Experiment::Part.find_by!(access_token: params[:part_id])
    @experiment = @current_part.experiment
  rescue ActiveRecord::RecordNotFound
    return send_json_status("Experiment's part not found", 404)
  end

  def find_variable_by(name)
    Experiment::Variable.find_by!(
      name: name,
      part_id: @current_part.id
    )
  rescue ActiveRecord::RecordNotFound
    send_json_status("Variable with name '#{name}' not found", 404) and return
  end

  #def check_part_state
  #  if @experiment.edit? || @experiment.closed?
  #    return send_json_status('Experiment is currently unavailable', 403)
  #  end
  #end

  def check_repetition_count
    @existing_data = Experiment::JsonDatum.where(
      part_id: @current_part.id,
      participant_id: @participant.id
    )

    if (@existing_data.size >= @current_part.repetition_count)
      return send_json_status('Variable repetition count is exceeded', 422)
    end
  end

  def variable_params
    #params.require(:part_id).permit(
      params.require(:part_id)
      params.permit(
      :part_id,
      :internal_id,
      :external_id,
      :format,
      variable_values: [:name, :value]
      )
    #params.permit(variable_values: [:name, :value]).to_h[:variable_values]
  end

  def set_cors_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers'] = '*'
    # response.headers['X-ComanyName-Api-Version'] = 'V1'
  end

  def ensure_json_request
    return if request.format == :json
    head :not_acceptable
  end

  def send_json_status(message, status)
    msg = { status: status, message: message }
    return render json: msg, status: status
  end

end
